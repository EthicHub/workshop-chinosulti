![logo](https://www.ethichub.com/hubfs/EthicHub%20template/Logo%20(4).svg)

# EthicHub workshop

This project belongs to a workshop we did at [cryptocafe](https://cryptocafe.madrid) (Calle Bastero 13, Madrid).

The smart contract implements game ["chinos"](https://es.wikipedia.org/wiki/Chinos_(juego)).

```shell
npx hardhat compile
npx hardhat test
npx hardhat run scripts/deploy.js
```

If you want to play: you need at least 1 ETHIX on Celo to play, and some Celo to pay for gas. Luck! :)

The smart contract on Celo Chain is: [0x6f58a18b4FCB513FCA971F11f42252D68Dc0775e](https://explorer.celo.org/address/0x6f58a18b4FCB513FCA971F11f42252D68Dc0775e/write-contract)
