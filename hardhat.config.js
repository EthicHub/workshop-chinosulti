require("@nomicfoundation/hardhat-toolbox");
require('dotenv').config();

let secrets = require('./.secrets.json');
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    compilers: [
      {
        version: '0.8.14',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          }
        },
      },
    ]
  },
  networks: {
    alfajores: {
      url: secrets.alfajores.node_url,
      chainId: 44787,
      accounts: {
        mnemonic: secrets.alfajores.mnemonic
      },
      gasPrice: 1000000000, // 1Gwei
    },
    celo: {
      url: secrets.celo.node_url,
      chainId: 42220,
      accounts: secrets.celo.pks,
      gasPrice: 1000000000, // 1Gwei
    },
  }
};
