// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");

async function main() {
  const limitOfParticipants = 3;
  const betAmount = "1000000000000000000"; // 1 Ethix
  const ETHIX_ADDRESS = "0x4620D7a5F58f77eeE69A38AfdAa8f2FfB10b42b6"; // alfajores
  //const ETHIX_ADDRESS = "0x9995cc8F20Db5896943Afc8eE0ba463259c931ed"; // celo
  //const ethixToken = await ethers.getContractAt('IERC20Upgradeable', ETHIX_ADDRESS);
  //const ethixToken = await Ethix.attach(ethixAddress);

  const ChinosUlti = await hre.ethers.getContractFactory("ChinosUlti");
  const chinosUlti = await ChinosUlti.deploy(limitOfParticipants, betAmount, ETHIX_ADDRESS);

  await chinosUlti.deployed();

  console.log("EthicHub ChinosUlti deployed to:", chinosUlti.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
