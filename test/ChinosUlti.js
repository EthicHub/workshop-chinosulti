/*
  Chinos Ulti test file.

  Copyright (C) 2022 EthicHub

  This file is part of EthicHub workshop.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const {
  time,
  loadFixture,
} = require("@nomicfoundation/hardhat-network-helpers");
const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
const { expect } = require("chai");
const parseEther = ethers.utils.parseEther

describe("ChinosUlti", function () {
  let owner, player1, player2, player3, chinosUlti, ethixToken;
  before(async function () {
    // Contracts are deployed using the first signer/account by default
    [owner, player1, player2, player3] = await ethers.getSigners();

    const EthixToken = await hre.ethers.getContractFactory("ERC20Token");
    ethixToken = await EthixToken.deploy('Ethix Token', 'ETHIX');
    await ethixToken.deployed();
    ethixToken.mint(player1.address, ethers.utils.parseEther('100'));
    ethixToken.mint(player2.address, ethers.utils.parseEther('100'));
    ethixToken.mint(player3.address, ethers.utils.parseEther('100'));

    const limitOfParticipants = 3;
    const betAmount = parseEther('1');
    const ChinosUlti = await hre.ethers.getContractFactory("ChinosUlti");
    chinosUlti = await ChinosUlti.deploy(limitOfParticipants, betAmount, ethixToken.address);
    await chinosUlti.deployed();

    await ethixToken.connect(player1).approve(chinosUlti.address, ethers.constants.MaxUint256);
    await ethixToken.connect(player2).approve(chinosUlti.address, ethers.constants.MaxUint256);
    await ethixToken.connect(player3).approve(chinosUlti.address, ethers.constants.MaxUint256);
  })
  beforeEach(async function () {})

  it('should be properly initialized', async function () {
    expect(await chinosUlti.limitOfParticipants()).to.equal(3);
    expect(await chinosUlti.betAmount()).to.equal(parseEther('1'));
  })

  describe("Play", function () {
    it("Should win player1", async function () {
      await expect(
        () => chinosUlti.connect(player1).play(2, 6)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player1.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('1'));
      await expect(
        () => chinosUlti.connect(player2).play(2, 3)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player2.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('2'));
      await expect(
        () => chinosUlti.connect(player3).play(2, 7)
      ).to.changeTokenBalance(ethixToken, player1, parseEther('3'));
      expect(await chinosUlti.isRegistered(player3.address)).to.be.false;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(0);

      // second play
      await expect(
        () => chinosUlti.connect(player1).play(2, 6)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player1.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('1'));
      await expect(
        () => chinosUlti.connect(player2).play(2, 3)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player2.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('2'));
      await expect(
        () => chinosUlti.connect(player3).play(2, 7)
      ).to.changeTokenBalance(ethixToken, player1, parseEther('3'));
      expect(await chinosUlti.isRegistered(player3.address)).to.be.false;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(0);
    });
    it("Should reject player1 is registered", async function () {
      await expect(
        () => chinosUlti.connect(player1).play(2, 8)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player1.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('1'));
      await expect(
        chinosUlti.connect(player1).play(2, 6)
      ).to.be.revertedWith('already registered');
    });
    it("Should reject player2 is select the same number than another player", async function () {
      expect(await chinosUlti.isRegistered(player1.address)).to.be.true;
      await expect(
        chinosUlti.connect(player2).play(2, 8)
      ).to.be.revertedWith('this bet is alrady selected');
    });
    it("Should reject player2 is select a number major than posible", async function () {
      expect(await chinosUlti.isRegistered(player1.address)).to.be.true;
      await expect(
        chinosUlti.connect(player2).play(2, 10)
      ).to.be.revertedWith('the bet must be minor than number of participants * 3');
    });
    it("Should win all tokens on the contract if the first play not win anybody", async function () {
      await expect(
        () => chinosUlti.connect(player2).play(2, 3)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player2.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('2'));
      await expect(
        () => chinosUlti.connect(player3).play(2, 7)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player3.address)).to.be.false;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('3'));

      // second play
      await expect(
        () => chinosUlti.connect(player1).play(2, 6)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player1.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('4'));
      await expect(
        () => chinosUlti.connect(player2).play(2, 3)
      ).to.changeTokenBalance(ethixToken, chinosUlti, parseEther('1'));
      expect(await chinosUlti.isRegistered(player2.address)).to.be.true;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(parseEther('5'));
      await expect(
        () => chinosUlti.connect(player3).play(2, 7)
      ).to.changeTokenBalance(ethixToken, player1, parseEther('6'));
      expect(await chinosUlti.isRegistered(player3.address)).to.be.false;
      expect(await ethixToken.balanceOf(chinosUlti.address)).to.be.equal(0);
    });
  });
  // TODO
  describe("Set new params", function () {});
});
